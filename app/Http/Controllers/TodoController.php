<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Todo as Todo;
use App\Http\Resources\Todo as TodoResource;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // fetch all tasks
        $todos = Todo::orderBy('status', 'asc')->orderBy('updated_at', 'desc')->get();
        return TodoResource::collection($todos);
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'task' => 'required|string|max:255', 
        ]);

        // return error when validation fails
        if ($validator->fails()) { 
            return response()->json(['status'=>'failed', 'errors'=>$validator->errors()->all()], 422);
        }

        $todo = Todo::create(['task'=>$request->task]);
        return response()->json([
            'status' => 'success', 
            'data' => new TodoResource($todo)
        ], 200);
    }

     

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todo = Todo::find($id);

        // return error when task doesn't exist
        if (!$todo) { 
            return response()->json(['status'=>'failed', 'message'=>'Task not found'], 404);
        }

        $todo->status = 1;
        $todo->save();
 
        return response()->json([
            'status' => 'success', 
            'data' => new TodoResource($todo)
        ], 200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::destroy($id); 
 
        return response()->json([
            'status' => 'success', 
            'message' => 'Task has been archived'
        ], 200); 
    }
}
